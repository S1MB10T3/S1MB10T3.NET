---
layout: art
title: "BUY IT. USE IT. BREAK IT. FIX IT."
image: build.gif
slug: "technologic"
order: 100
---

![](/assets/img/technologic/light.jpg)

In the process of creating my own "typeface" and decide to put it into practice.
It's more like it's own language than a actual English font.

Each page is a line from Daft Punk's _Technologic_

- Nickel Plate and Wire

![](/assets/img/technologic/cover.jpg)
![](/assets/img/technologic/page.jpg)
![](/assets/img/technologic/stand.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/D8K90hX4PrE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
