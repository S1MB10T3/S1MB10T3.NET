---
title: BUGS NYU
layout: project
logo: bugs/bugs.png
image: bugs/bugs.png
slug: bugsnyu
order: 100
---

An open source website for an open source club!
[Git Repo](https://github.com/S1MB10T3/bugs-nyu.github.io)

## About the Club.
BUGS is focused on the promotion of open source by engaging students in open source projects on the local and global scale, hosting speakers who work with open source in industry and philanthropy, and running events to advocate for open standards in subjects such as data and innovation.

# Web Design

![]({{ site.baseurl }}/assets/img/bugs/bugshome.png)
