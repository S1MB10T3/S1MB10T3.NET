---
layout: art
title: "Cyborgs. Robots. and Automotons."
image: cyborgs.gif
slug: "robot"
order: 2
---

![](/assets/img/cyborg/cyborg cover.jpg)

## Original
Story of a depressed cyborg telling it's story through song lyrics.

- Letter Press
- Font - Caslon 12pt
- Created April 10, 2018


![](/assets/img/cyborg/cyborg open.jpg)
![](/assets/img/cyborg/cyborg front.jpg)
![](/assets/img/cyborg/cyborg page.jpg)


## Redux
Same story but better...
Sold during the Gallatin Half Print Fair.

![](/assets/img/cyborg/redux stand.jpg)

- Screen Printed
- Font - Caslon 12pt
- Created March 29th, 2019


![](/assets/img/cyborg/redux side.jpg)
![](/assets/img/cyborg/redux page.jpg)
![](/assets/img/cyborg/redux cover.jpg)
