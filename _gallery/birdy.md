---
layout: art
title: "To ashes"
image: birdy.gif
slug: "birdy"
order: 4
---

<iframe src="https://player.vimeo.com/video/383399121" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Before
![](/assets/img/birdy/flying.jpg)
![](/assets/img/birdy/finger.jpg)
![](/assets/img/birdy/front.jpg)
![](/assets/img/birdy/top.jpg)

## After
![](/assets/img/birdy/burned-front.jpg)
![](/assets/img/birdy/burned-sid.jpg)
![](/assets/img/birdy/burned-top.jpg)
